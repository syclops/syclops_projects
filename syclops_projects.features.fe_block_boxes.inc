<?php
/**
 * @file
 * syclops_projects.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function syclops_projects_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Archived Message';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'archived_message';
  $fe_block_boxes->body = '<center>
<p>&nbsp;</p>
<h1>This content has been archived</h1>
<hr />
<p>
This content has been archived by a project manager or site administrator. Please contact the concerned person if you need this content restored.
</p>
<p>&nbsp;</p>
<p>
<h4>If you are a project manager or administrator</h4>
Use the <span class="label label-default"><i class="icon-briefcase"></i> Restore</span> link above to restore this content and make it accessible to project members.
</p>

</center>';

  $export['archived_message'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Page Summary Block';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'summary_block';
  $fe_block_boxes->body = '<?php 
   $node = menu_get_object();
   syclops_ui_page_summary_block_generator($node); 
 ?>';

  $export['summary_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Page Summary Block(Dashboard)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'summary_block_other_page';
  $fe_block_boxes->body = '<?php
  $content = syclops_ui_other_page_summary_block_generator();
 print  \'<div class="row">
          <div class="pull-left syclops-summary-block-title col-sm-6">
              <h2>My Projects</h2>
          </div>
         <div class="pull-right">
            <span class="icon-stack icon-3x pull-right"  title="My Project">
               <i class="icon-sign-blank icon-stack-base yellow-green"></i>
               <i class="icon-folder-close icon-light"></i>
            </span>
          </div>
       </div>\';
  if($content) {
  print \'<div class="row"><table class="table table-condensed table-hover">
    <thead class="text-muted text-center">
        <tr>
             <th style="width : 50%;" title="Projects">Project</th>
             <th class="text-center" title="Connections">
              <span class="icon-stack" style="font-size : 70%;">
               <i class="icon-sign-blank icon-stack-base"></i>
                <i class="icon-terminal icon-light"></i>
              </span>
             </th>
             <th class="text-center" title="Grants"><i class="icon-key"></i></th>
             <th class="text-center" title="Sessions"><i class="icon-youtube-play"></i></th>
             <th class="text-center" title="Users"><i class="icon-user"></i></th>
         </tr>
     </thead><tbody class="text-center">\';
  foreach($content as $value) {
    print \'<tr>
           <td style="text-align : left;"><small><a href=" \'.strtolower(str_replace(" ", "-", $value[\'title\'])).\' ">\'.$value[\'title\'].\'</small></td>
           <td>\'.$value[\'connection\'].\'</td>
           <td>\'.$value[\'grant\'].\'</td>
           <td>\'.$value[\'session\'].\'</td>
           <td>\'.$value[\'user\'].\'</td>
       </tr>\';
   }
 print \'</tbody></table></div>\'; 
 }  else {
  print "<p>You don\'t have any project.</p>";
}
?>';

  $export['summary_block_other_page'] = $fe_block_boxes;

  return $export;
}
