<?php
/**
 * @file
 * syclops_projects.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function syclops_projects_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-archived_message'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'archived_message',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-summary_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'summary_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-summary_block_other_page'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'summary_block_other_page',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_og-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_og-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_project-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_project-block_2'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_project-block_3'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_project-block_4'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_project-block_6'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_2'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_3'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}
