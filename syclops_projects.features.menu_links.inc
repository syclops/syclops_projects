<?php
/**
 * @file
 * syclops_projects.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function syclops_projects_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation:node/add/project
  $menu_links['navigation:node/add/project'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/project',
    'router_path' => 'node/add/project',
    'link_title' => 'Project',
    'options' => array(
      'attributes' => array(
        'title' => 'Group of users who work on a specific project.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Project');


  return $menu_links;
}
