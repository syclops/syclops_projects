<?php
/**
 * @file
 * syclops_projects.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function syclops_projects_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function syclops_projects_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function syclops_projects_node_info() {
  $items = array(
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Group of users who work on a specific project.'),
      'has_title' => '1',
      'title_label' => t('Project Name'),
      'help' => '',
    ),
  );
  return $items;
}
